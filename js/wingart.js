 	$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" id="myViewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ

//  window.onload = function () {
// 	if(screen.width <= 617) {
// 	    var mvp = document.getElementById('myViewport');
// 	    mvp.setAttribute('content','width=617');
// 	}
// }


    var owl   		= $(".slider"),
		isotopeBox  = $(".masonry_box"),
    	fancybox    = $(".fancybox");
    	// tabs 		= $(".tabs");

    //----Include----
	    if(owl.length){
	    	include("js/owl.carousel.js");
	    }

	    if(fancybox.length){
	    	include("../../js/jquery.fancybox.js");
	    	// include("js/jquery.fancybox-buttons.js");
	    	// include("js/jquery.fancybox-thumbs.js");
	    }

	    if(isotopeBox.length){
			include("../../js/isotope.pkgd.js");
			include("../../js/packery-mode.pkgd.js");
		}

		// if(tabs.length){
		// 	include("plugins/easy-responsive-tabs/easyResponsiveTabs.js");
		// }

		include("../../js/modernizr.js");

    //----Include-Function----
	    function include(url){
	      document.write('<script src="'+ url + '"></script>');
	    }


$(document).ready(function(){


	/* ------------------------------------------------
	CAROUSEL START
	------------------------------------------------ */

	if(owl.length){
		owl.owlCarousel({
			singleItem : true,
			items : 4,
			smartSpeed:1000,
			nav: false,
			dot: true
		});
	}

	/* ------------------------------------------------
	CAROUSEL END
	------------------------------------------------ */



	/* ------------------------------------------------
	ANCHOR START
	------------------------------------------------ */
			 
		    function goUp(){
				var windowHeight = $(window).height(),
					windowScroll = $(window).scrollTop();

				if(windowScroll>windowHeight/2){
					$('.arrow_up').addClass('active animated fadeInRight');
				}

				else{
					$('.arrow_up').removeClass('active animated fadeInRight');
					$('.arrow_up').removeClass('click animated fadeOutRight');
				}

		    }

		    goUp();
			$(window).on('scroll',goUp);

			$('.arrow_up').on('click ontouchstart',function () {

				if($.browser.safari){
					$('body').animate( { scrollTop: 0 }, 1100 );
					$('.arrow_up').addClass('click animated fadeOutRight');
				}
				else{
					$('html,body').animate( { scrollTop: 0}, 1100 );
					$('.arrow_up').addClass('click animated fadeOutRight');
				}
				return false;
				
			});

	/* ------------------------------------------------
	ANCHOR END
	------------------------------------------------ */



	/* ------------------------------------------------
	FANCYBOX START
	------------------------------------------------ */
			 

	        if(fancybox.length){
				fancybox.fancybox();
			}

	/* ------------------------------------------------
	FANCYBOX END
	------------------------------------------------ */



	/* ------------------------------------------------
	MASONRY START
	------------------------------------------------ */
			 
	    if(isotopeBox.length){
		  	$(isotopeBox).isotope({
			  	layoutMode: 'packery',
		    	itemSelector: '.masonry_item'
			})
		};

	/* ------------------------------------------------
	MASONRY END
	------------------------------------------------ */



	/* ------------------------------------------------
	ACCORDION START
	------------------------------------------------ */

			// if(tabs.length){
			// 	$( function() {
			//         $( ".tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
			//         $( ".tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
			//     });
			// }


	/* ------------------------------------------------
	ACCORDION END
	------------------------------------------------ */



	/* ------------------------------------------------
	RESPONSIVE MENU START
	------------------------------------------------ */

			$(".resp_btn, .close_resp_menu").on("click ontouchstart", function(){
				$("body").toggleClass("show_menu")
			});

			$(document).on("click ontouchstart", function(event) {
				if ($(event.target).closest("nav,.resp_btn").length) return;
				$("body").removeClass("show_menu");
				if($(window).width() <= 768){
					$(".menu_item").removeClass("active").find(".dropdown_menu").css("display","none");
				}
			    event.stopPropagation();
		    });

			// проверка на наличие элемента и вставка хтмл кода
		 	 // if($(window).width() <= 767){
				if ($(".menu_item").length){
			        if($(".menu_item").has('.dropdown_menu')){
			        	var $this = $(".menu_item"),
			        		dropdownMenu = $this.find(".dropdown_menu");

			        		dropdownMenu.parent($this).find(".menu_link").append('<span class="dropdown_arrow"><span class="dropdown_arrow_item"></span></span>');
			        }
			    }
			// }
			// проверка на наличие элемента и вставка хтмл кода


			$('.menu_link').on('click ontouchstart',function(event){
				if($("html").hasClass("md_no-touch"))return;

		        var windowWidth = $(window).width(),
		            $parent = $(this).parent('.menu_item');
		        if(windowWidth > 768){
		          // if($("html").hasClass("md_touch")){
		            if((!$parent.hasClass('active')) && $parent.find('.dropdown_menu').length){

		              event.preventDefault();

		              $parent.toggleClass('active')
		               .siblings()
		               .find('.menu_link')
		               .removeClass('active');
		            }
		          // }  
		        }
		        
		        else{
		            
		          if((!$parent.hasClass('active')) && $parent.find('.dropdown_menu').length){

		            event.preventDefault();

		            $parent.toggleClass('active')
		             .siblings()
		             .removeClass('active');
		            $parent.find(".dropdown_menu")
		             .slideToggle()
		             .parents('.menu_item')
		             .siblings()
		             .find(".dropdown_menu")
		             .slideUp();
		          }
		        }

		    });

	/* ------------------------------------------------
	RESPONSIVE MENU END
	------------------------------------------------ */



	/* ------------------------------------------------
	ACCORDION START
	------------------------------------------------ */

			if($('.accordion_item_link').length){
				$('.accordion_item_link').on('click', function(){
					$(this)
					.toggleClass('active')
					.next('.sub-menu')
					.slideToggle()
					.parents(".accordion_item")
					.siblings(".accordion_item")
					.find(".accordion_item_link")
					.removeClass("active")
					.next(".sub-menu")
					.slideUp();
				});  
			}

	/* ------------------------------------------------
	ACCORDION END
	------------------------------------------------ */


	/* ------------------------------------------------
	ШИРИНА БЛОКА ПОКАЗЫВАЕТЬСЯ НА ЭКРАН START
	------------------------------------------------ */

			// var div      = document.createElement('div');
			// 	cssObj   = {"position": "fixed","bottom": "50px","right": "50px","font-size": "20px","color": "black"};
			// div.className = "windowWidth";
			// $(div).css(cssObj);
			// document.body.appendChild(div)


			// function windowWidth(target){
			// 	var widthDiv = $(window).width();
			// 	$("body").find(".windowWidth").text(widthDiv + scrollWidth);
			// 	// console.log(widthDiv);
			// }
			// windowWidth();

			// $(window).on('resize',function(){
			// 	windowWidth();
			// });

    /* ------------------------------------------------
	ШИРИНА БЛОКА ПОКАЗЫВАЕТЬСЯ НА ЭКРАН END
	------------------------------------------------ */
});
